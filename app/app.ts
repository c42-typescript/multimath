function startGame() {
    // starting new game
    var messagesElement = document.getElementById('messages');
    messagesElement!.innerText = 'Welcome to Multimath! Starting new game!...';
    console.log("Started new game");
}

document.getElementById('startGame')!.addEventListener('click', startGame);